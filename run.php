<?php
require_once __DIR__ . '/vendor/autoload.php';

use TeamTNT\TNTSearch\TNTSearch;

$tnt = new TNTSearch;

$tnt->loadConfig([
    'driver' => 'mysql',
    'host' => 'localhost:8889',
    'database' => 'naonik19',
    'username' => 'root',
    'password' => 'root',
    'storage' => join(DIRECTORY_SEPARATOR, [dirname(__FILE__), "local_index", ""]),
    'stemmer' => \TeamTNT\TNTSearch\Stemmer\PorterStemmer::class//optional
]);

$indexer = $tnt->createIndex('books.index');
$indexer->query('SELECT isbn, title, subtitle, author FROM ad_book;');
$indexer->setPrimaryKey('isbn');
$indexer->includePrimaryKey();
//$indexer->setLanguage('italian');
$indexer->run();

