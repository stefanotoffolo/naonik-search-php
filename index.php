<?php
require_once __DIR__ . '/vendor/autoload.php';

use TeamTNT\TNTSearch\TNTSearch;
use Symfony\Component\HttpFoundation\Request;


$app = new Silex\Application();
$app->post('/search', function (Request $request) use ($app) {
    $q = $request->get('query');

    $tnt = new TNTSearch;
    $tnt->loadConfig([
        'storage' => join(DIRECTORY_SEPARATOR, [dirname(__FILE__), "local_index", ""]),
        'stemmer' => \TeamTNT\TNTSearch\Stemmer\PorterStemmer::class
    ]);

    $tnt->fuzziness = true;

    $tnt->selectIndex("books.index");
    $res = $tnt->search($q, 64);
    return $app->json($res);
});

$app->run();